> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Terri Howze

### Assignment 2 Requirements:

*Four parts:*

    1.Payroll Calculator
    2. Chapter questions (Chs 3,4)
    3. Bitbucket repo links
        a)this assignment and
        b)the completed tutorial(bitbucketstationlocations)
    4. Skillsets
   

#### README.md file should include the following items:
    *Screenshots payroll app running
    *Screenshots of skill sets

[Payroll Calclator Jupyter notebook](a2_payroll_calculator/payroll_calculator.ipynb"A2 Jupyter Notebook")

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:
*Payroll with Overtime*:   |*Payroll without Overtime*:
:-------------------------:|:-------------------------:
![Payroll with overtime screenshot](img/a2_payroll_calculator_vs_code.PNG) | ![Payroll without overtime screenshot](img/a2_tip_calculator_idle.PNG)

### Skill set Screenshots

*Skill Set 1*:   |*Skill Set 2*:       |*Skill Set 3*:
:-------------------------:|:-------------------------:|:-------------------------:
![skill 1 screenshot](img/skill_1.PNG) |![skill 2 screenshot](img/skill_2.PNG) | ![skill 3 screenshot](img/skill_3.PNG) 

### Jupyter Notebook Screenshots

 Screenshot 1 | Screenshot 2 | Screensh0t 3 
:-------------------------:|:-------------------------:|:-------------------------:
 ![Jupyter notebook screenshot 1](img/a2_jup_notebook_1.PNG) | ![Jupyter notebook screenshot 2](img/a2_jup_notebook_2.PNG) | ![Jupyter notebook screenshot 3](img/a2_jup_notebook_3.PNG)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/fsuthowze/bitbucketstationlocations/src/master/)

#### Bitbucket Links:

*LIS4369*
[LIS4369 remote repo link](https://bitbucket.org/fsuthowze/lis4369/src/master/)



