> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Terri Howze

### Assignment 4 Requirements:

*Four parts:*

    1. Data Analysis 2
    2. Chapter questions
    3. Bitbucket repo links
    4. Skillsets
   

#### README.md file should include the following items:
    *Screenshots  Data Annalysis 2 app running
    *Screenshot of graph
    *Screenshots of skill sets

[Data Analysis 2 Jupyter notebook](a4_data_anal_2/a4_data_anal_2.ipynb"A$ Jupyter Notebook")

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:
*Data Analysis*:   
![Data Analysis screenshot](img/data_anal_2.PNG) |

#### Graph Screenshot
![Graph Screenshot](img/graph.PNG)

### Skill set Screenshots

*Skill Set 10*:   |*Skill Set 11*:       |*Skill Set 12*:
:-------------------------:|:-------------------------:|:-------------------------:
![skill 10 screenshot](img/skill_set_10.PNG) |![skill 11 screenshot](img/skill_set_11.PNG) | ![skill 12 screenshot](img/skill_set_12.PNG) 

### Jupyter Notebook Screenshots

 Screenshot 1 | Screenshot 2 | Screensh0t 3 
:-------------------------:|:-------------------------:|:-------------------------:
 ![Jupyter notebook screenshot 1](img/jup_note_1.PNG) | ![Jupyter notebook screenshot 2](img/jup_note_2.PNG) | ![Jupyter notebook screenshot 3](img/jup_note_3.PNG)


#### Bitbucket Links:

*LIS4369*
[LIS4369 remote repo link](https://bitbucket.org/fsuthowze/lis4369/src/master/)



