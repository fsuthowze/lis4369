#! python3

# Course: LIS4369
# Semester: Fall 2020


def get_requirements():

    print("Developer : Terri Howze")
    print("IT/ICT Student Percentage")
    print("\nProgram Requirements:\n"
        + "1. Find number of IT/ICT students in class.\n"
        + "2. Calculate IT/ICT Student Percentage"
        + "3. Muust use float data type(to facilitate right-alignment).\n"
        + "4. Format, right-align numbers, and round two decimal places.\n")

def calculate_it_ict_student_percentage():

    #initialize variables
    it = 0
    ict = 0
    total_students = 0
    percent_it = 0
    percent_ict = 0

    #get user data
    print("Input:")
    it = int(input("Enter number of IT students: "))
    ict = int(input("Enter number of ICT students: "))

    #calculate total number of students
    total_students = it + ict

    #calculate percent of IT students
    percent_it = it/total_students

    #calculate percent of ICT students
    percent_ict = it/total_students

    #print output
    print("\nOutput:")
    print("{0:17} {1:>5.2f}".format("Total Students:", total_students))
    print("{0:17} {1:>5.2%}".format("IT Students:", percent_it))
    print("{0:17} {1:>5.2%}".format("ICT Students:", percent_ict))
