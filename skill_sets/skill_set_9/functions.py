#! python3
# Course: LIS4369
# Semester: Fall 2020


def get_requirements():

    print("Developer : Terri Howze")
    print("Python Sets - like mathematical sets!")
    print("\nProgram Requirements:\n"
        + "1. Print while loop.\n"
        + "2. Print for loops using range() function, and implicit and explicit lists.\n"
        + "3. Use break and continue statements.\n"
        + "4. Replicate display below.\n")

def using_sets():

    print("\nInput: Hard coded--no user input. See three examples above")
    print("***Note***: All three sets below print as  \"sets\" (i.e., curly brackets, *regardless* of how they were created!\n")

    my_set = {1, 3.14, 2.0, 'four', 'Five'}
    print(my_set)

    print("\nPrint type of my_set")
    print(type(my_set))

    my_set1 = set([1, 3.14, 2.0, 'four', 'Five'])
    print("\nPrint my_set1 created using set() function with list: ")
    print(my_set1)

    print("\nPrint type of my_set1: ")
    print(type(my_set1))

    my_set2 = set((1, 3.14, 2.0, 'four', 'Five'))
    print("\nPrint my_set2 created using set() function with tuples: ")
    print(my_set2)

    print("\nPrint type of my_set2: ")
    print(type(my_set2))

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nDiscard 'four':")
    my_set.discard('four')
    print(my_set)

    print("\nRemove 'Five'")
    my_set.remove('Five')
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nAdd elements to set(4) using add() method:")
    my_set.add(4)
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nDisplay minimun number:")
    print(min(my_set))

    print("\nDisplay maximum number:")
    print(max(my_set))

    print("\nDisplay sum ofnumber:")
    print(sum(my_set))

    print("\nDelete all set elements:")
    my_set.clear()
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))
