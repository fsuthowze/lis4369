import functions as f 

def main():
    f.get_rquirements()
    f.get_valid_float()
    f.get_valid_operator()
    f.error_handling()

if __name__ == "__main__":
    main