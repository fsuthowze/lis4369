> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4369 - Extensible Enterprise Solutions
# Python/R Data Analytics/Visualization)
## Terri Howze

### Assignments:

*Course Work Links:*

1.  [A1 README.md](a1/README.md "My A1 README.md file")
    * Install Python
    * Install R
    * Install R Studio
    * Install Visual Studio Code
    * Create *a1_tip_calculator* application
    * Create *a1 tip calculator* Jupyter Notebook
    * Provide Screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorial (bitbucketstationlocations)
    * Provide git command descriptions

2.  [A2 README.md](a2/README.md "My A2 README.md file")
    * Create Payroll app using "Separation of Concerns" design principles
    * Provide screenshots of completed app
    * Provide screenshots of completed Python skill sets

3.  [A3 README.md](a3/README.md "My A3 README.md file")
    * Painting Estimator 
    * Provide screenshots of completed app
    * Provide screenshots of completed Python skill sets

4.  [A4 README.md](a4/README.md "My A4 README.md file")
    * Data Analysis 2 
    * Provide screenshots of completed app
    * Provide screenshots of completed Python skill sets

5.  [A5 README.md](a5/README.md "My A5 README.md file")
    * Introduction to R setup and tutorial
    * Combine demo1.R and demo2.R into lis4369_a5.R
    * Screenshots of lis4369_a5.R
    * Screenshots of learn_to_use_r.R
    * SCreenshots of Python skill setsS

### Projects:

6.  [P1 README.md](p1/README.md "My P1 README.md file")
    

7.  [P2 README.md](p2/README.md "My P2 README.md file")
    - lis4369 p2 requirements
    - Screenshots of RStudio
    - At least 2 graphs with name