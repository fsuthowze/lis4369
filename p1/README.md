> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Terri Howze

### Project 1 Requirements:

*Four parts:*

    1. Data Analysis
    2. Chapter questions
    3. Bitbucket repo links
    4. Skillsets
   

#### README.md file should include the following items:
    *Screenshots  Data Annalysis app running
    *Screenshot of graph
    *Screenshot of demo.py
    *Screenshots of skill sets

[Data Analysis Jupyter notebook](p1_data_anal/p1_data_anal.ipynb"P1 Jupyter Notebook")

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:
*Data Analysis*:   |*Data Analysis Continued*:
:-------------------------:|:-------------------------:
![Data Analysis screenshot](img/data_anal_1.PNG) | ![Data Analysis continued screenshot](img/data_anal_2.PNG)

#### Graph Screenshot
![Graph Screenshot](img/graph.PNG)

### Skill set Screenshots

*Skill Set 7*:   |*Skill Set 8*:       |*Skill Set 9*:
:-------------------------:|:-------------------------:|:-------------------------:
![skill 7 screenshot](img/ss7.PNG) |![skill 8 screenshot](img/ss8.PNG) | ![skill 9 screenshot](img/ss9.PNG) 

### Jupyter Notebook Screenshots

 Screenshot 1 | Screenshot 2 | Screensh0t 3 
:-------------------------:|:-------------------------:|:-------------------------:
 ![Jupyter notebook screenshot 1](img/jup_note_1.PNG) | ![Jupyter notebook screenshot 2](img/jup_note_2.PNG) | ![Jupyter notebook screenshot 3](img/jup_note_3.PNG)


#### Bitbucket Links:

*LIS4369*
[LIS4369 remote repo link](https://bitbucket.org/fsuthowze/lis4369/src/master/)



