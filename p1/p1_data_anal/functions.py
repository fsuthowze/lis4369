#! python3
#Developer : Terri Howze
# Course: LIS4369
# Semester: Fall 2020


import datetime
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from matplotlib import style

def get_requirements():

    print("Data Analysis 1")
    print("\nProgram Requirements: ")
    print("\n1. Run demo.py.")
    print("\n2. If errors, more than likely missing installitions.")
    print("\n3. Test Python Package Installer: pip freeze")
    print("\n4. Research how to do the following installitions:")
    print("\n\ta. pandas (only if missing)")
    print("\n\tb. pandas-datareader (only if missing)")
    print("\n\tc. matplotlib (only if missing")
    print("\n5. Create at least three functions that are called by the program:")
    print("\n\ta. main(): calls at leasr two other functions.")
    print("\n\tb. get_requirements(): displays the program requirements.")
    print("\n\tc. data_analysis_1(): displays the following data.")


def data_analysis_1():
    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime.now()

    df = pdr.DataReader("XOM", "yahoo", start, end)

    print("\nPrint number of records: ")
    print(len(df))

    print("\nPrint columns")
    print(df.columns)

    print("\nPrint data frame: ")
    print(df)

    print("\nPrint first five lines:")
    print(df.head())

    print("\nPrint last five lines: ")
    print(df.tail())

    print("\nPrint first 2 lines: ")
    print(df.head(2))

    print("\nPrint last 2 lines: ")
    print(df.tail(2))

    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()