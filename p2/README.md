> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Terri Howze

### Project 2 Requirements:

*Three parts:*

    1. Rstudio
    2. Chapter questions
    3. Bitbucket repo links
    
   

#### README.md file should include the following items:
    *Screenshots  RStudio executing code
    *Screenshot of graph with name
  

[Project 2 RStudio Link](p2/lis4369_p2.Rb"P2 RStudio link")

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:
*RStudio*:   
![Rstudio Screenshot](img/rp1.PNG) |

#### Graph Screenshot
*Graph 1*:   |*Graph 2*: |       
:-------------------------:|:-------------------------:|
![Graph 1 screenshot](img/plot_disp_and_mpg_1.png) |![Graph 2 screenshot](img/plot_disp_and_mpg_2.png) | 




#### Bitbucket Links:

*LIS4369*
[LIS4369 remote repo link](https://bitbucket.org/fsuthowze/lis4369/src/master/)



