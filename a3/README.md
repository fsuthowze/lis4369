> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Terri Howze

### Assignment 3 Requirements:

*Four parts:*

    1. Painting Estimator
    2. Chapter questions
    3. Bitbucket repo links
    4. Skillsets
   

#### README.md file should include the following items:
    *Screenshots  Estimator app running
    *Screenshots of skill sets

[Painting Estimator Jupyter notebook](a3_painting_estimator/a3_painting_estimator.ipynb"A3 Jupyter Notebook")

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:
*Painting Estimator*:   |*Painting Estimator Continued*:
:-------------------------:|:-------------------------:
![Painting estimator screenshot](img/a3_estimate_vscode_p1.PNG) | ![Painting Estimator continued screenshot](img/a3_estimate_vscode_p2.PNG)

### Skill set Screenshots

*Skill Set 4*:   |*Skill Set 5*:       |*Skill Set 6*:
:-------------------------:|:-------------------------:|:-------------------------:
![skill 4 screenshot](img/skill_4.PNG) |![skill 5 screenshot](img/skill51.PNG)![skill 5 screenshot](img/skill52.PNG) | ![skill 6 screenshot](img/skill6.PNG) 

### Jupyter Notebook Screenshots

 Screenshot 1 | Screenshot 2 | Screensh0t 3 
:-------------------------:|:-------------------------:|:-------------------------:
 ![Jupyter notebook screenshot 1](img/jup_note_1.PNG) | ![Jupyter notebook screenshot 2](img/jup_note_2.PNG) | ![Jupyter notebook screenshot 3](img/jup_note_3.PNG)


#### Bitbucket Links:

*LIS4369*
[LIS4369 remote repo link](https://bitbucket.org/fsuthowze/lis4369/src/master/)



